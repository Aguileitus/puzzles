# Puzzles

1. Can you solve this logic puzzle?

```
A1 B1 C1 D1 E1 F1
A2 B2 C2 D2 E2 F2
A3 B3 C3 D3 E3 F3
A4 B4 C4 D4 E4 F4
A5 B5 C5 D5 E5 F5
A6 B6 C6 D6 E6 F6
```

- Organize every row, every column and the two diagonals to contain one of each letter and number
- For example A1 and A5 cannot be on the same row, column or diagonal because they share a letter
- For example B3 and C3 cannot be on the same row, column or diagonal because they share a number
