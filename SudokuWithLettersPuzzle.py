"""Puzzles"""

from datetime import datetime

class Puzzle:
    """Puzzle class"""

    def __init__(self, size, iter_print, /):
        self.x_len = size
        self.y_len = size

        self.used = set()
        self.iterations = 0
        self.iter_print = iter_print

    def get_iterations(self, /):
        """Gets interations"""
        return self.iterations

    def print_matrix(self, matrix, /):
        """Prints a matrix"""
        print("  ", end="")
        for i in range(self.y_len):
            print(f"  {i}", end="")
        print()

        for i in range(self.x_len):
            print(f"  -{'---' * self.y_len}\n{i} ", end="")
            for j in range(self.y_len):
                print(f"|{matrix[i][j]:2}", end="")
            print("|")
        print(f"  -{'---' * self.y_len} ")

    def find_empty(self, matrix, /):
        """Find next empty cell"""
        for i in range(self.x_len):
            for j in range(self.y_len):
                if matrix[i][j] == "--":
                    return (i, j) #row, col
        return None

    def valid_position(self, matrix, item, x, y, debug=False, /):
        """Validates if item can be in matrix[x][y]"""
        if debug:
            print(f"Valid {item} in ({x}, {y})?", end=" ")

        #Check if item is used to avoid duplicates
        if item in self.used:
            if debug:
                print("Dupped", False)
            return False

        letter, number = item

        #Check row
        for j in range(self.y_len):
            elem = matrix[x][j]
            if (elem[0] == letter or elem[1] == number) and y != j:
                if debug:
                    print("In row", False)
                return False

        #Check column
        for i in range(self.x_len):
            elem = matrix[i][y]
            if (elem[0] == letter or elem[1] == number) and x != i:
                if debug:
                    print("In column", False)
                return False

        #Check diag
        if x == y:
            for i in range(self.x_len):
                elem = matrix[i][i]
                if (elem[0] == letter or elem[1] == number) and ((x, y) != (i, i)):
                    if debug:
                        print("In diag", False)
                    return False

        #Check inverse diag
        if (x + y) == (self.x_len - 1):
            for i in range(self.x_len):
                elem = matrix[i][self.x_len - i - 1]
                if (elem[0] == letter or elem[1] == number) and (x, y) != (self.x_len - i - 1):
                    if debug:
                        print("In diag", False)
                    return False

        if debug:
            print(True)
        return True

    def solve(self, matrix, items, debug=False, /):
        """Solver"""
        if debug:
            input()
            print(f"Solve: {items}")
            print(f"Used: {self.used}")

        self.iterations += 1
        if self.iterations % self.iter_print == 0:
            print(f"Iteration: {self.iterations}")
            self.print_matrix(matrix)

        empty = self.find_empty(matrix)
        if debug:
            print(f"Empty: {empty}")
        if not empty or (len(items) == len(self.used)):
            return True

        x, y = empty

        for item in items:
            if self.valid_position(matrix, item, x, y, debug):
                matrix[x][y] = item
                self.used.add(item)

                if self.solve(matrix, items, debug):
                    return True

                if debug:
                    print(f"Restore {item} at ({x}, {y})")
                matrix[x][y] = "--"
                self.used.remove(item)

        return False

def generate_items(n):
    """Generates the list of valid items"""
    if n > 9:
        return []

    letters = ["A", "B", "C", "D", "E", "F", "G", "H", "G"]
    items = []

    for i in range(n):
        for j in range(n):
            items.append(f"{letters[j]}{i + 1}")

    return items

def time_taken(start_time, end_time, brief=False):
    """
    Returns a formated string with the uptime info
    """
    delta = end_time - start_time

    days = delta.days
    hours, _ = divmod(int(delta.total_seconds()), 3600)
    minutes = int(delta.total_seconds() / 60)
    seconds = delta.seconds
    ms = delta.microseconds

    if not brief:
        fmt = ''
        if days:
            fmt += f'{days} days, '
        if hours:
            fmt += f'{hours} hours, '
        if minutes:
            fmt = f'{minutes} minutes, '
        fmt = f'{seconds} seconds, {ms:06} microseconds'
    else:
        fmt = ''
        if days:
            fmt += f'{days}d, '
        if hours:
            fmt += f'{hours}h, '
        if minutes:
            fmt = f'{minutes}m, '
        fmt = f'{seconds}s, {ms:06}ms'

    return fmt

def main():
    """
    Can you solve this logic puzzle?

    A1 B1 C1 D1 E1 F1
    A2 B2 C2 D2 E2 F2
    A3 B3 C3 D3 E3 F3
    A4 B4 C4 D4 E4 F4
    A5 B5 C5 D5 E5 F5
    A6 B6 C6 D6 E6 F6

    Organize every row, every column and the two diagonals to contain one of each letter and number
    For example A1 and A5 cannot be on the same row, column or diagonal because they share a letter
    For example B3 and C3 cannot be on the same row, column or diagonal because they share a number
    """
    size = 5

    items = generate_items(size)
    matrix = [["--" for _ in range(size)] for _ in range(size)]

    p = Puzzle(size, 10000)

    print(f"Items: {items}")
    print("Start:")
    p.print_matrix(matrix)
    start_time = datetime.utcnow()
    if p.solve(matrix, items, False):
        print("Solution:")
    else:
        print("There are no solutions:")
    end_time = datetime.utcnow()
    p.print_matrix(matrix)
    print("Iterations: ", p.get_iterations())
    print("Time taken: ", time_taken(start_time, end_time, brief=True))

if __name__ == "__main__":
    main()
